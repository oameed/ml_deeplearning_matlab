#!/bin/bash

cd run/matlab

echo   ' MAKE DIRECTORY FOR EXPERIMENT '                'v11'
rm     -rf                                ../../networks/v11
tar    -xzf  ../../networks/v00.tar.gz -C ../../networks
mv           ../../networks/v00           ../../networks/v11

echo   ' START TRAINING FOR EXPERIMENT '                'v11'
matlab -nodisplay -nosplash -nodesktop -r "train_classifier('classifier','v11','mnist',2,128);exit;"

echo ' GENERATING PREDICTIONS '
matlab -nodisplay -nosplash -nodesktop -r "predict('classifier','v11','mnist',{});exit;"


