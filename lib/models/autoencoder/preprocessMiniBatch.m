%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% DEEP LEARNING WITH MATLAB                           %%%
%%% MODEL FUNCTIONS                                     %%%
%%% AUTOENCODER BATCH PROCESSING                        %%%
%%% by: OAMEED NOAKOASTEEN                              %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function X=preprocessMiniBatch(Xcell)
X=cat(3,Xcell{:})                             ;
X=reshape(X,[size(X,1),size(X,2),1,size(X,3)]);
end
