%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% DEEP LEARNING WITH MATLAB                           %%%
%%% UTILITY FUNCTIONS                                   %%%
%%% GET LABEL FROM SOFTMAX                              %%%
%%% by: OAMEED NOAKOASTEEN                              %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function preds=get_label_from_softmax(PREDS)
preds        =[]             ;
for i=1:size(PREDS,2)
    [~,index]=max(PREDS(:,i));
    preds    =[preds;index-1];
end

end
