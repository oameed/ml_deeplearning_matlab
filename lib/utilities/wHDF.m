%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% DEEP LEARNING WITH MATLAB                           %%%
%%% UTILITY FUNCTIONS                                   %%%
%%% WRITE HDF FILES                                     %%%
%%% by: OAMEED NOAKOASTEEN                              %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function wHDF(FILENAME,DIR,DATA)

for index=1:size(DIR,2)
    h5create(FILENAME,fullfile('/',DIR{index}),size(DATA{index}));
    h5write (FILENAME,fullfile('/',DIR{index}),DATA{index}      );
end

end
