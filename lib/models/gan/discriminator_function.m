%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% DEEP LEARNING WITH MATLAB                           %%%
%%% MODEL FUNCTIONS                                     %%%
%%% DISCRIMINATOR FUNCTION                              %%%
%%% by: OAMEED NOAKOASTEEN                              %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [x,STATE]=discriminator_function(PARAMETERS,X,doTraining,STATE)


w        =PARAMETERS.discriminator.conv1.Weights                               ;
b        =PARAMETERS.discriminator.conv1.Bias                                  ;

x        =layer_conv2d         (X,w     ,b    ,2   ,'same'                  )  ;

offset   =PARAMETERS.discriminator.batchnorm1.Offset                           ;
scale    =PARAMETERS.discriminator.batchnorm1.Scale                            ;
Mean     =STATE.discriminator.batchnorm1.TrainedMean                           ;
Variance =STATE.discriminator.batchnorm1.TrainedVariance                       ;
if doTraining
    [x,Mean,Variance]               =batchnorm(x,offset,scale,Mean,Variance )  ;
    STATE.discriminator.batchnorm1.TrainedMean    =Mean                        ;
    STATE.discriminator.batchnorm1.TrainedVariance=Variance                    ;
else
    x                               =batchnorm(x,offset,scale,Mean,Variance )  ;
end

x        =relu(x)                                                              ;

w        =PARAMETERS.discriminator.conv2.Weights                               ;
b        =PARAMETERS.discriminator.conv2.Bias                                  ;

x        =layer_conv2d         (x,w     ,b    ,2   ,'same'                  )  ;

offset   =PARAMETERS.discriminator.batchnorm2.Offset                           ;
scale    =PARAMETERS.discriminator.batchnorm2.Scale                            ;
Mean     =STATE.discriminator.batchnorm2.TrainedMean                           ;
Variance =STATE.discriminator.batchnorm2.TrainedVariance                       ;
if doTraining
    [x,Mean,Variance]               =batchnorm(x,offset,scale,Mean,Variance )  ;
    STATE.discriminator.batchnorm2.TrainedMean    =Mean                        ;
    STATE.discriminator.batchnorm2.TrainedVariance=Variance                    ;
else
    x                               =batchnorm(x,offset,scale,Mean,Variance )  ;
end

x        =relu(x)                                                              ;

w        =PARAMETERS.discriminator.fc1.Weights                                 ;
b        =PARAMETERS.discriminator.fc1.Bias                                    ;

x        =layer_fullyconnected(x,w     ,b                                   )  ; 

end
