%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% DEEP LEARNING WITH MATLAB                           %%%
%%% MODEL FUNCTIONS                                     %%%
%%% GAN LOSS                                            %%%
%%% by: OAMEED NOAKOASTEEN                              %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [d_loss,g_loss]=model_loss(REAL,FAKE)

d_loss =-mean(log(REAL)) - mean(log(1-FAKE));
g_loss =-mean(log(FAKE))                    ;

end
