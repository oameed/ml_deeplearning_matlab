%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% DEEP LEARNING WITH MATLAB                           %%%
%%% UTILITY FUNCTIONS                                   %%%
%%% LOAD FROM CHECKPOINT                                %%%
%%% by: OAMEED NOAKOASTEEN                              %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function filename=get_checkpoint_name(PATH,KEY)
temp             =[]                                         ;
filenames        =get_filenames(fullfile(PATH,'checkpoints'));
for i=1:size(filenames,2)
    filename     =filenames{i}                               ;
    splits       =split(filename,'_')                        ;
    splits       =splits{end}                                ;
    splits       =split(splits  ,'.')                        ;
    temp         =[temp,str2num(splits{1})]                  ;
end
if strcmp(KEY,'last')
    [~,index]    =max(temp)                                  ;
else
    if strcmp(KEY,'first')
        [~,index]=min(temp)                                  ;
    end
end
filename         =filenames{index}                           ;

end
