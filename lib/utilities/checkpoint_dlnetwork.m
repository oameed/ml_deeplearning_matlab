%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% DEEP LEARNING WITH MATLAB                           %%%
%%% UTILITY FUNCTIONS                                   %%%
%%% CHECKPOINT                                          %%%
%%% by: OAMEED NOAKOASTEEN                              %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function checkpoint_dlnetwork(model,EPOCH,PATH)

ckpntame=strcat  ('checkpoint','_'          ,'epoch' ,'_',num2str(EPOCH),'.mat');
filename=fullfile(PATH        ,'checkpoints',ckpntame                          );
cleanup( fullfile(PATH,'checkpoints'))
save   ( filename,'model')

end
