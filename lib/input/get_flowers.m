%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% DEEP LEARNING WITH MATLAB                           %%%
%%% INPUT FUNCTIONS                                     %%%
%%% FLOWERS                                             %%%
%%% by: OAMEED NOAKOASTEEN                              %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function get_flowers(PATH)
addpath(PATH)
dirname ="flower_photos"                                                  ;
filename=strcat(dirname,".tgz")                                           ;
url     =strcat("http://download.tensorflow.org/example_images/",filename);
if ~ exist (fullfile(PATH,dirname ),"dir")
    disp("Downloading Flowers dataset ...")
    websave(fullfile(PATH,filename),url )
    untar  (fullfile(PATH,filename),PATH)
end
end
