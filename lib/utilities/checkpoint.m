%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% DEEP LEARNING WITH MATLAB                           %%%
%%% UTILITY FUNCTIONS                                   %%%
%%% CHECKPOINT                                          %%%
%%% by: OAMEED NOAKOASTEEN                              %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function checkpoint(parameters,state,EPOCH,PATH)

ckpntame=strcat  ('checkpoint','_'          ,'epoch' ,'_',num2str(EPOCH),'.mat');
filename=fullfile(PATH        ,'checkpoints',ckpntame                          );
cleanup( fullfile(PATH,'checkpoints'))
save   ( filename,'parameters','state')

end
