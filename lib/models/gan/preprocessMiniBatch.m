%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% DEEP LEARNING WITH MATLAB                           %%%
%%% MODEL FUNCTIONS                                     %%%
%%% GAN BATCH PROCESSING                                %%%
%%% by: OAMEED NOAKOASTEEN                              %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function X=preprocessMiniBatch(Xcell)
X = cat(4,Xcell{:});
X = rescale(X,-1,1,InputMin=0,InputMax=1);
end
