%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% DEEP LEARNING WITH MATLAB                           %%%
%%% LAYER FUNCTIONS                                     %%%
%%% CONVTRANSPOSE2D WEIGHTS                             %%%
%%% by: OAMEED NOAKOASTEEN                              %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [W,B]=layer_convtranspose2d_weights(FILTER,IN,OUT)
W=initializeGlorot([FILTER,OUT,IN]  ,...
                   prod(FILTER) * IN,...
                   prod(FILTER) * IN     );
B=initializeZeros ([OUT 1]               );

end
