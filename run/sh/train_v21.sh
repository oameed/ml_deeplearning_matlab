#!/bin/bash

cd run/matlab

echo   ' MAKE DIRECTORY FOR EXPERIMENT '                'v21'
rm     -rf                                ../../networks/v21
tar    -xzf  ../../networks/v00.tar.gz -C ../../networks
mv           ../../networks/v00           ../../networks/v21

echo   ' START TRAINING FOR EXPERIMENT '                'v21'
matlab -nodisplay -nosplash -nodesktop -r "train_autoencoder('autoencoder','v21','mnist',1,128);exit;"

echo ' GENERATING PREDICTIONS '
matlab -nodisplay -nosplash -nodesktop -r "predict('autoencoder','v21','mnist');exit;"


