%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% DEEP LEARNING WITH MATLAB                           %%%
%%% MODEL FUNCTIONS                                     %%%
%%% CLASSIFIER BATCH PROCESSING                         %%%
%%% by: OAMEED NOAKOASTEEN                              %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [X,Y]=preprocessMiniBatch(Xcell,Ycell)
X=cat(3,Xcell{:})                             ;
X=reshape(X,[size(X,1),size(X,2),1,size(X,3)]);
Y=cat(1,Ycell{:})                             ;
Y=onehotencode(categorical(Y'),1)             ;
end
