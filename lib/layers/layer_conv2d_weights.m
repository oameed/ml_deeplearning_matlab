%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% DEEP LEARNING WITH MATLAB                           %%%
%%% LAYER FUNCTIONS                                     %%%
%%% CONV2D WEIGHTS                                      %%%
%%% by: OAMEED NOAKOASTEEN                              %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% FILTER: DIMENSIONS OF THE FILTER
% IN    : NUMBER OF INPUT  CHANNELS
% OUT   : NUMBER OF OUTPUT CHANNELS  

function [W,B]=layer_conv2d_weights(FILTER,IN,OUT)
W=initializeGlorot([FILTER,IN,OUT]   ,...
                   prod(FILTER) * OUT,...
                   prod(FILTER) * OUT    );
B=initializeZeros ([OUT 1]               );
end
