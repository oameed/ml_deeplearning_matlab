%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% DEEP LEARNING WITH MATLAB                           %%%
%%% INPUT FUNCTIONS                                     %%%
%%% PROCESS DATASET                                     %%%
%%% by: OAMEED NOAKOASTEEN                              %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function data_process(DATASET,MODE,PATH)

filename_samples  =strcat('samples','.png')                     ;
filename_processed=strcat( MODE    ,'.h5' )                     ;

if strcmp(DATASET,'mnist')
    [img,lab]     =get_mnist(fullfile(PATH,'raw'),MODE)         ;
    plotter('grid'                                   ,...
            img(:,:,:,randperm(size(img,4),100))     ,...
            fullfile(PATH,'samples',filename_samples),...
            {}                                               )
    cleanup(fullfile(PATH,'processed')                       )
    wHDF   (fullfile(PATH,'processed',filename_processed),...
            {'img','lab'}                                ,...
            { img , lab }                                    )
else
    if strcmp(DATASET,'flowers')
        dirname  ="flower_photos"                                         ;
        dirnames =["daisy","dandelion","roses","sunflowers","tulips"]     ;
        FILENAMES=[]                                                      ;
        get_flowers(fullfile(PATH,'raw'))
        
        for i=1:size(dirnames,2)
            path_to_files=fullfile     (PATH,'raw',dirname,dirnames(i)  ) ;
            filenames    =get_filenames(path_to_files                   ) ;
            filenames    =filenames    (:,randperm(size(filenames,2),20)) ;
            filenames    =fullfile     (path_to_files,filenames         ) ;
            FILENAMES    =[FILENAMES,filenames]                           ;
        end
        imshow(imtile(FILENAMES))
        saveas(gcf                                      ,...
               fullfile(PATH,'samples',filename_samples),...
               'png'                                        )
        close all
    end
end

end

