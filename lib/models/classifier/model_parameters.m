%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% DEEP LEARNING WITH MATLAB                           %%%
%%% MODEL FUNCTIONS                                     %%%
%%% CLASSIFIER PARAMETERS                               %%%
%%% by: OAMEED NOAKOASTEEN                              %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [parameters,state]=model_parameters()
[w,b]                           = layer_conv2d_weights   ([3 3],1 ,16); % FIRST  CONVOLUTION & BATCH NORM LAYERS
parameters.conv1.Weights        =w                                    ;
parameters.conv1.Bias           =b                                    ; 

[offset,scale,mean,variance]    = layer_batchnorm_weights(16         );
parameters.batchnorm1.Offset    =offset                               ;
parameters.batchnorm1.Scale     =scale                                ;
state.batchnorm1.TrainedMean    =mean                                 ;
state.batchnorm1.TrainedVariance=variance                             ;

[w,b]                           = layer_conv2d_weights   ([3 3],16,32); % SECOND CONVOLUTION & BATCH NORM LAYERS
parameters.conv2.Weights        =w                                    ;
parameters.conv2.Bias           =b                                    ; 

[offset,scale,mean,variance]    = layer_batchnorm_weights(32         );
parameters.batchnorm2.Offset    =offset                               ;
parameters.batchnorm2.Scale     =scale                                ;
state.batchnorm2.TrainedMean    =mean                                 ;
state.batchnorm2.TrainedVariance=variance                             ;

[w,b]                           =layer_fullyconnected_weights(1568,10); % FIRST FULLY CONNECTED           LAYER
parameters.fc1.Weights          =w                                    ;
parameters.fc1.Bias             =b                                    ;
end
