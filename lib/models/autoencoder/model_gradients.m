%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% DEEP LEARNING WITH MATLAB                           %%%
%%% MODEL FUNCTIONS                                     %%%
%%% AUTOENCODER GRADIENTS                               %%%
%%% by: OAMEED NOAKOASTEEN                              %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [gradients,STATE,loss]=model_gradients(PARAMETERS,X,STATE)

[output,STATE] =model_function(PARAMETERS,X         ,true, STATE);
loss           =mse           (output    ,X                     );
gradients      =dlgradient    (loss      ,PARAMETERS            );

end
