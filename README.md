# Deep Learning with MATLAB's [Deep Learning Toolbox](https://www.mathworks.com/products/deep-learning.html)

## References

[[ 1 ].](https://www.mathworks.com/help/deeplearning/ug/train-network-using-model-function.html) _Train Network Using Model Function_  
[[ 2 ].](https://www.mathworks.com/help/deeplearning/ug/make-predictions-using-model-function.html) _Make Predictions Using Model Function_  
[[ 3 ].](https://www.mathworks.com/help/deeplearning/ug/define-custom-training-loops-loss-functions-and-networks.html) _Define Custom Training Loops, Loss Functions, and Networks_  
[[ 4 ].](https://www.mathworks.com/help/deeplearning/ug/data-sets-for-deep-learning.html) _Data Sets for Deep Learning_  


_Tutorials_

[[ 5 ].](https://www.mathworks.com/help/deeplearning/ug/train-generative-adversarial-network.html) _Train Generative Adversarial Network (GAN)_  
[[ 6 ].](https://www.mathworks.com/help/deeplearning/ug/train-conditional-generative-adversarial-network.html) _Train Conditional Generative Adversarial Network (CGAN)_  
[[ 7 ].](https://www.mathworks.com/help/deeplearning/ug/trainwasserstein-gan-with-gradient-penalty-wgan-gp.html) _Train Wasserstein GAN with Gradient Penalty (WGAN-GP)_  
[[ 8 ].](https://www.mathworks.com/help/deeplearning/ug/monitor-gan-training-progress-and-identify-common-failure-modes.html) _Monitor GAN Training Progress and Identify Common Failure Modes_  
[[ 9 ].](https://www.mathworks.com/help/deeplearning/ug/sequence-to-sequence-translation-using-attention.html) _Sequence-to-Sequence Translation Using Attention_  
[[10].](https://www.mathworks.com/help/deeplearning/ug/generate-text-using-autoencoders.html) _Generate Text Using Autoencoders_  

## Acknowledgements

* We would like to thank the University of New Mexico Center for Advanced Research Computing [(CARC)](http://carc.unm.edu/), supported in part by the National Science Foundation, for providing the high performance computing resources used in this work.


## Code Statistics
<pre>
github.com/AlDanial/cloc v 1.82  T=0.05 s (989.3 files/s, 31840.8 lines/s)
-------------------------------------------------------------------------------
Language                     files          blank        comment           code
-------------------------------------------------------------------------------
MATLAB                          47            270            283            971
Markdown                         1             28              0             68
Bourne Shell                     6             40             14             64
-------------------------------------------------------------------------------
SUM:                            54            338            297           1103
-------------------------------------------------------------------------------
</pre>

## How To Run

* This project uses `MATLAB` Version `R2021a` along with `Deep Learning Toolbox` Version `14.2` and `Parallel Computing Toolbox` Version `7.4`.
* MNIST dataset must be downloaded manually and placed in `data/mnist/raw`
* To run experiments:
  * on local PC (with training a classifier as an example):
    1. `cd` to main project directory.
    2. `./run/sh/train_v11.sh`
  * on HPC system (with training a GAN as an example):
    1. `cd` to main project directory
    2. `cd ./run/slrm/`
    3. `sbatch train_v31.sh`

## Datasets

|     |     |
|:---:|:---:|
**[MNIST](http://yann.lecun.com/exdb/mnist/)**  | **[Flowers](https://www.tensorflow.org/datasets/catalog/tf_flowers)**
![][fig_1] | ![][fig_0]

[fig_0]:data/flowers/samples/samples.png
[fig_1]:data/mnist/samples/samples.png

## Experiment v11: Classifying MNIST

|     |     |
|:---:|:---:|
![][fig_2] | ![][fig_3]

[fig_2]:networks/v11/logs/train/metrics.png
[fig_3]:networks/v11/predictions/predictions.png

## Experiment v21: Autoencoder

|     |     |
|:---:|:---:|
![][fig_4] | ![][fig_5]

[fig_4]:networks/v21/logs/train/metrics.png
[fig_5]:networks/v21/predictions/predictions.png


## Experiment v31: GAN

|     |     |
|:---:|:---:|
![][fig_6] | ![][fig_7]

[fig_6]:networks/v31/logs/train/metrics.png
[fig_7]:networks/v31/predictions/predictions.png

## Experiment v32: GAN

|     |     |
|:---:|:---:|
![][fig_8] | ![][fig_9]

[fig_8]:networks/v32/logs/train/metrics.png
[fig_9]:networks/v32/predictions/predictions.png

