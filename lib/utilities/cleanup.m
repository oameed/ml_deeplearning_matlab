%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% DEEP LEARNING WITH MATLAB                           %%%
%%% UTILITY FUNCTIONS                                   %%%
%%% CLEAN UP                                            %%%
%%% by: OAMEED NOAKOASTEEN                              %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function cleanup(PATH)

filenames=get_filenames(PATH);
if ~ isempty(filenames)
    for i=1:size(filenames,2)
        filename=fullfile(PATH,filenames(i));
        delete           (     filename    )
    end
end

end
