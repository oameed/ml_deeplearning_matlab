%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% DEEP LEARNING WITH MATLAB                           %%%
%%% PREDICT                                             %%%
%%% by: OAMEED NOAKOASTEEN                              %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function predict(MODEL,VER,DATASET,CONFIG)
close all
clc

addpath     (fullfile(matlabroot,'examples' ,'nnet'    ,'main'        )    )
addpath     (fullfile('..'      ,'..'       ,'lib'     ,'input'       ),...
             fullfile('..'      ,'..'       ,'lib'     ,'layers'      ),...
             fullfile('..'      ,'..'       ,'lib'     ,'models',MODEL),...
             fullfile('..'      ,'..'       ,'lib'     ,'utilities'   )    )
PATHS      ={fullfile('..'      ,'..'       ,'data'    , DATASET      ),...
             fullfile('..'      ,'..'       ,'networks', VER          )    }                     ;

filename   =get_checkpoint_name         (         PATHS{2},'last'                                );
load                                    (fullfile(PATHS{2},'checkpoints',filename)               )  %%% LOAD MODEL PARAMETERS

if strcmp(MODEL,'classifier')
    [img,label]  =get_mnist             (fullfile(PATHS{1},'raw'),'test'                         ); %%% LOAD DATASET
    [x  ,~    ]  =model_function        (parameters              ,dlarray(img,'SSCB'),false,state); %%% MAKE PREDICTIONS
    label_preds  =get_label_from_softmax(double(extractdata(x))                                  ); 
    plotter            ('cm'                                                           ,...                   
                        {label,label_preds}                                            ,...
                        fullfile(PATHS{2}  ,'predictions',strcat('predictions','.png')),...
                        {}                                                                       )  %%% EXPORT PREDICTIONS
else
    if strcmp(MODEL,'autoencoder')
        [img,~]  =get_mnist             (fullfile(PATHS{1},'raw'),'test'                         ); %%% LOAD DATASET
        [x  ,~]  =model_function        (parameters              ,dlarray(img,'SSCB'),false,state); %%% MAKE PREDICTIONS
        plotter        ('grid'                                                         ,...
                        double(extractdata(x(:,:,:,randperm(size(x,4),100))))          ,...
                        fullfile(PATHS{2},'predictions',strcat('predictions','.png'))  ,...
                        {}                                                                       )  %%% EXPORT PREDICTIONS
    else
        if strcmp(MODEL,'gan')
            Z    =noise(100,100);
            [x,~]=generator_function    (parameters              ,dlarray(Z  ,'CB'  ),false,state); %%% MAKE PREDICTIONS
            plotter    ('grid'                                                         ,...
                        double(extractdata(x))                                         ,...
                        fullfile(PATHS{2},'predictions',strcat('predictions','.png'))  ,...
                        {}                                                                       )  %%% EXPORT PREDICTIONS
        else
            if strcmp(MODEL,'gan_from_tutorial')
                Z=noise(100,100);
                x=predict(model,dlarray(Z,'CB'));
                plotter('grid'                                                         ,...
                        double(extractdata(x))                                         ,...
                        fullfile(PATHS{2},'predictions',strcat('predictions','.png'))  ,...
                        {}                                                                       )  %%% EXPORT PREDICTIONS
            end
        end
    end
end


end
