%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% DEEP LEARNING WITH MATLAB                           %%%
%%% UTILITY FUNCTIONS                                   %%%
%%% GAN TRAINING PROGRESS                               %%%
%%% by: OAMEED NOAKOASTEEN                              %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [lineLossGenerator    ,...
          lineLossDiscriminator,...
          imageAxes                ]=training_progress_gan()

fig                  =figure                                          ;
fig.Position(3)      =2*fig.Position(3)                               ;
imageAxes            =subplot(1,2,1)                                  ;
scoreAxes            =subplot(1,2,2)                                  ;
lineLossGenerator    =animatedline(scoreAxes,Color='r');
lineLossDiscriminator=animatedline(scoreAxes,Color='b');
legend("Generator","Discriminator")                                   ;
ylim  ([0 inf]                    )
xlabel("Iteration"                )
ylabel("Loss"                     )
grid on

end
