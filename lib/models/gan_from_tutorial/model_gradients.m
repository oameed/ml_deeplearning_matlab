%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% DEEP LEARNING WITH MATLAB                           %%%
%%% MODEL FUNCTIONS                                     %%%
%%% GAN GRADIENTS                                       %%%
%%% by: OAMEED NOAKOASTEEN                              %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [gradientsGenerator    ,...
          gradientsDiscriminator,...
          STATE_G               ,...
          scoreGenerator        ,...
          scoreDiscriminator        ]=model_gradients(GENERATOR    ,...
                                                      DISCRIMINATOR,...
                                                      Z            ,...
                                                      X            ,...
                                                      FLIP             )

[img_fake  ,STATE_G]=forward(GENERATOR    ,Z       );
logit_real          =forward(DISCRIMINATOR,X       );
logit_fake          =forward(DISCRIMINATOR,img_fake);

prob_real             =sigmoid               (logit_real                         );
prob_fake             =sigmoid               (logit_fake                         );

scoreDiscriminator    =(mean(prob_real) + mean(1-prob_fake)) / 2;
scoreGenerator        =                   mean(  prob_fake)     ;

index                 =randperm(size(prob_real,4),floor(FLIP * size(prob_real,4)))                ;
prob_real(:,:,:,index)=1-prob_real(:,:,:,index)                                                   ;

[d_loss,g_loss]       =model_loss            (prob_real ,prob_fake                               );

gradientsGenerator    =dlgradient            (g_loss    ,GENERATOR.Learnables    ,RetainData=true);
gradientsDiscriminator=dlgradient            (d_loss    ,DISCRIMINATOR.Learnables                );

end
