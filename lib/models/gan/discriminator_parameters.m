%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% DEEP LEARNING WITH MATLAB                           %%%
%%% MODEL FUNCTIONS                                     %%%
%%% DISCRIMINATOR PARAMETERS                            %%%
%%% by: OAMEED NOAKOASTEEN                              %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [parameters,state]=discriminator_parameters()

[w,b]                                         =layer_conv2d_weights         ([3 3],1 ,16);
parameters.discriminator.conv1.Weights        =w                                         ;
parameters.discriminator.conv1.Bias           =b                                         ;

[offset,scale,mean,variance]                  =layer_batchnorm_weights      (16         );
parameters.discriminator.batchnorm1.Offset    =offset                                    ;
parameters.discriminator.batchnorm1.Scale     =scale                                     ;
state.discriminator.batchnorm1.TrainedMean    =mean                                      ;
state.discriminator.batchnorm1.TrainedVariance=variance                                  ;


[w,b]                                         =layer_conv2d_weights         ([3 3],16,32);
parameters.discriminator.conv2.Weights        =w                                         ;
parameters.discriminator.conv2.Bias           =b                                         ;

[offset,scale,mean,variance]                  =layer_batchnorm_weights      (32         );
parameters.discriminator.batchnorm2.Offset    =offset                                    ;
parameters.discriminator.batchnorm2.Scale     =scale                                     ;
state.discriminator.batchnorm2.TrainedMean    =mean                                      ;
state.discriminator.batchnorm2.TrainedVariance=variance                                  ; %%%


[w,b]                                         =layer_fullyconnected_weights (1568 ,1    );
parameters.discriminator.fc1.Weights          =w                                         ;
parameters.discriminator.fc1.Bias             =b                                         ;

end
