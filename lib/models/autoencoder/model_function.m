%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% DEEP LEARNING WITH MATLAB                           %%%
%%% MODEL FUNCTIONS                                     %%%
%%% AUTOENCODER FUNCTION                                %%%
%%% by: OAMEED NOAKOASTEEN                              %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [x,STATE]=model_function(PARAMETERS,X,doTraining,STATE)

w        =PARAMETERS.conv1.Weights                                             ;
b        =PARAMETERS.conv1.Bias                                                ;

x        =layer_conv2d         (X,w     ,b    ,2   ,'same'                  )  ; % FIRST  CONVOLUTION

offset   =PARAMETERS.batchnorm1.Offset                                         ;
scale    =PARAMETERS.batchnorm1.Scale                                          ;
Mean     =STATE.batchnorm1.TrainedMean                                         ;
Variance =STATE.batchnorm1.TrainedVariance                                     ;
if doTraining
    [x,Mean,Variance]               =batchnorm(x,offset,scale,Mean,Variance )  ;
    STATE.batchnorm1.TrainedMean    =Mean                                      ;
    STATE.batchnorm1.TrainedVariance=Variance                                  ;
else
    x                               =batchnorm(x,offset,scale,Mean,Variance )  ;
end

x        =relu(x)                                                              ;

w        =PARAMETERS.conv2.Weights                                             ;
b        =PARAMETERS.conv2.Bias                                                ;

x        =layer_conv2d         (x,w     ,b    ,2   ,'same'                  )  ; % SECOND CONVOLUTION

offset   =PARAMETERS.batchnorm2.Offset                                         ;
scale    =PARAMETERS.batchnorm2.Scale                                          ;
Mean     =STATE.batchnorm2.TrainedMean                                         ;
Variance =STATE.batchnorm2.TrainedVariance                                     ;
if doTraining
    [x,Mean,Variance]               =batchnorm(x,offset,scale,Mean,Variance )  ;
    STATE.batchnorm2.TrainedMean    =Mean                                      ;
    STATE.batchnorm2.TrainedVariance=Variance                                  ;
else
    x                               =batchnorm(x,offset,scale,Mean,Variance )  ;
end

x        =relu(x)                                                              ;


w        =PARAMETERS.convt1.Weights                                            ;
b        =PARAMETERS.convt1.Bias                                               ;

x        =layer_convtranspose2d(x,w     ,b         ,[[0,0];[1,1]]           )  ; % FIRST  CONVTRANSPOSE

offset   =PARAMETERS.batchnorm3.Offset                                         ;
scale    =PARAMETERS.batchnorm3.Scale                                          ;
Mean     =STATE.batchnorm3.TrainedMean                                         ;
Variance =STATE.batchnorm3.TrainedVariance                                     ;
if doTraining
    [x,Mean,Variance]               =batchnorm(x,offset,scale,Mean,Variance )  ;
    STATE.batchnorm3.TrainedMean    =Mean                                      ;
    STATE.batchnorm3.TrainedVariance=Variance                                  ;
else
    x                               =batchnorm(x,offset,scale,Mean,Variance )  ;
end

x        =relu(x)                                                              ;

w        =PARAMETERS.convt2.Weights                                            ;
b        =PARAMETERS.convt2.Bias                                               ;

x        =layer_convtranspose2d(x,w     ,b         ,[[0,0];[1,1]]           )  ; % SECOND CONVTRANSPOSE

end
