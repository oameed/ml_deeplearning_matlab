%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% DEEP LEARNING WITH MATLAB                           %%%
%%% UTILITY FUNCTIONS                                   %%%
%%% GET FILE NAMES                                      %%%
%%% by: OAMEED NOAKOASTEEN                              %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function filenames=get_filenames(PATH)
filenames={}                           ;
FILENAMES=string({dir(PATH).name})     ;

counter  =0                            ;

for i=1:size(FILENAMES,2)
    if ~(startsWith(FILENAMES(i),'.'))
        counter           =counter+1   ;
        filenames{counter}=FILENAMES(i);
    end
end

filenames=string(filenames);

end
