%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% DEEP LEARNING WITH MATLAB                           %%%
%%% INPUT FUNCTIONS                                     %%%
%%% MNIST                                               %%%
%%% by: OAMEED NOAKOASTEEN                              %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [img,label]=get_mnist(PATH,MODE)
addpath(PATH)
filenameImagesTrain='train-images-idx3-ubyte.gz';
filenameLabelsTrain='train-labels-idx1-ubyte.gz';
filenameImagesTest ='t10k-images-idx3-ubyte.gz' ;
filenameLabelsTest ='t10k-labels-idx1-ubyte.gz' ;
if strcmp    (MODE,'train')
    name_img       =filenameImagesTrain         ;
    name_label     =filenameLabelsTrain         ;
else
    if strcmp(MODE,'test' )
        name_img   =filenameImagesTest          ;
        name_label =filenameLabelsTest          ;
    end
end
img  =processImagesMNIST(name_img     )         ;
label=processLabelsMNIST(name_label   )         ;
img  =extractdata       (img          )         ;
label=double            (string(label))         ;
end
