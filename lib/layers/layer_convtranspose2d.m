%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% DEEP LEARNING WITH MATLAB                           %%%
%%% LAYER FUNCTIONS                                     %%%
%%% CONVTRANSPOSE2D                                     %%%
%%% by: OAMEED NOAKOASTEEN                              %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function x=layer_convtranspose2d(X,W,B,CROPPING)
x=dltranspconv(X,W,B,'Stride',2,'Cropping',CROPPING);
end
