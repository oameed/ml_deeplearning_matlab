%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% DEEP LEARNING WITH MATLAB                           %%%
%%% UTILITY FUNCTIONS                                   %%%
%%% PLOTTER                                             %%%
%%% by: OAMEED NOAKOASTEEN                              %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function plotter(MODE,DATA,FILENAME,CONFIG)
if strcmp(MODE,'grid')
    imshow(imtile(DATA),'Border','tight')
else
    if strcmp(MODE,'log')
        FILENAME=fullfile(FILENAME,'logs','train',strcat('metrics','.png'));
    else
        if strcmp(MODE,'cm')
            label      =DATA{1};
            label_preds=DATA{2};
            confusionchart(label,label_preds)
        end
    end
end

saveas(gcf,FILENAME,'png')
close all

end
