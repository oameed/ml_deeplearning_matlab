%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% DEEP LEARNING WITH MATLAB                           %%%
%%% MODEL FUNCTIONS                                     %%%
%%% CLASSIFIER GRADIENTS                                %%%
%%% by: OAMEED NOAKOASTEEN                              %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [gradients,STATE,loss]=model_gradients(PARAMETERS,X,Y,STATE)

[output,STATE] =model_function(PARAMETERS,X         ,true, STATE);
loss           =crossentropy  (output    ,Y                     );
gradients      =dlgradient    (loss      ,PARAMETERS            );

end
