%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% DEEP LEARNING WITH MATLAB                           %%%
%%% LAYER FUNCTIONS                                     %%%
%%% FULLY CONNECTED WEIGHTS                             %%%
%%% by: OAMEED NOAKOASTEEN                              %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% IN    : NUMBER OF INPUT  CHANNELS
% OUT   : NUMBER OF OUTPUT CHANNELS 

function [W,B]=layer_fullyconnected_weights(IN,OUT)
W=initializeGlorot([OUT IN],...
                    OUT    ,...
                    IN         );
B=initializeZeros ([OUT 1]     );
end
