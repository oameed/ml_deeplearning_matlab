#!/bin/bash

#SBATCH --ntasks=8
#SBATCH --time=48:00:00
#SBATCH --partition=singleGPU
#SBATCH --gres=gpu:1
#SBATCH --mail-type=BEGIN,FALI,END
#SBATCH --mail-user=
#SBATCH --job-name=GAN

module load matlab

cd      $SLURM_SUBMIT_DIR
cd      ../matlab

echo   ' MAKE DIRECTORY FOR EXPERIMENT '                'v31'
rm     -rf                                ../../networks/v31
tar    -xzf  ../../networks/v00.tar.gz -C ../../networks
mv           ../../networks/v00           ../../networks/v31

echo   ' START TRAINING FOR EXPERIMENT '                'v31'
matlab -nodisplay -nosplash -nodesktop -r "train_gan('gan','v31','mnist',20,128);exit;"

echo ' GENERATING PREDICTIONS '
matlab -nodisplay -nosplash -nodesktop -r "predict('gan','v31','mnist');exit;"


