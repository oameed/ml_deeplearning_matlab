%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% DEEP LEARNING WITH MATLAB                           %%%
%%% MODEL FUNCTIONS                                     %%%
%%% GAN GRADIENTS                                       %%%
%%% by: OAMEED NOAKOASTEEN                              %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [gradientsGenerator    ,...
          gradientsDiscriminator,...
          STATE_G               ,...
          STATE_D               ,...
          scoreGenerator        ,...
          scoreDiscriminator        ]=model_gradients(PARAMETERS_G,...
                                                      PARAMETERS_D,...
                                                      Z           ,...
                                                      X           ,...
                                                      STATE_G     ,...
                                                      STATE_D     ,...
                                                      FLIP            )

[img_fake  ,STATE_G]  =generator_function    (PARAMETERS_G,Z       ,true,STATE_G );
[logit_real,STATE_D]  =discriminator_function(PARAMETERS_D,X       ,true,STATE_D );
[logit_fake,STATE_D]  =discriminator_function(PARAMETERS_D,img_fake,true,STATE_D );

prob_real             =sigmoid               (logit_real                         );
prob_fake             =sigmoid               (logit_fake                         );

scoreDiscriminator    =(mean(prob_real) + mean(1-prob_fake)) / 2;
scoreGenerator        =                   mean(  prob_fake)     ;

index                 =randperm(size(prob_real,4),floor(FLIP * size(prob_real,4)));
prob_real(:,:,:,index)=1-prob_real(:,:,:,index)                                         ;

[d_loss,g_loss]       =model_loss            (prob_real ,prob_fake               );

gradientsGenerator    =dlgradient            (g_loss    ,PARAMETERS_G            );
gradientsDiscriminator=dlgradient            (d_loss    ,PARAMETERS_D            );

end
