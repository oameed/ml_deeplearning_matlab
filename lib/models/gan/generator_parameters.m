%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% DEEP LEARNING WITH MATLAB                           %%%
%%% MODEL FUNCTIONS                                     %%%
%%% GENERATOR PARAMETERS                                %%%
%%% by: OAMEED NOAKOASTEEN                              %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [parameters,state]=generator_parameters()

[w,b]                                         =layer_fullyconnected_weights (100,prod([7 7 64]));
parameters.generator.fc1.Weights              =w                                         ;
parameters.generator.fc1.Bias                 =b                                         ;

[offset,scale,mean,variance]                  =layer_batchnorm_weights      (    prod([7 7 64]));
parameters.generator.batchnorm1.Offset        =offset                                    ;
parameters.generator.batchnorm1.Scale         =scale                                     ;
state.generator.batchnorm1.TrainedMean        =mean                                      ;
state.generator.batchnorm1.TrainedVariance    =variance                                  ;


[w,b]                                         =layer_convtranspose2d_weights([3,3],64,32);
parameters.generator.convt1.Weights           =w                                         ;
parameters.generator.convt1.Bias              =b                                         ;

[offset,scale,mean,variance]                  =layer_batchnorm_weights      (32         );
parameters.generator.batchnorm2.Offset        =offset                                    ;
parameters.generator.batchnorm2.Scale         =scale                                     ;
state.generator.batchnorm2.TrainedMean        =mean                                      ;
state.generator.batchnorm2.TrainedVariance    =variance                                  ;

[w,b]                                         =layer_convtranspose2d_weights([3,3],32,1 );
parameters.generator.convt2.Weights           =w                                         ;
parameters.generator.convt2.Bias              =b                                         ;

[offset,scale,mean,variance]                  =layer_batchnorm_weights      (1          );
parameters.generator.batchnorm3.Offset        =offset                                    ;
parameters.generator.batchnorm3.Scale         =scale                                     ;
state.generator.batchnorm3.TrainedMean        =mean                                      ;
state.generator.batchnorm3.TrainedVariance    =variance                                  ;

end
