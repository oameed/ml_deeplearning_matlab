%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% DEEP LEARNING WITH MATLAB                           %%%
%%% TRAIN                                               %%%
%%% by: OAMEED NOAKOASTEEN                              %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function train_autoencoder(MODEL,VER,DATASET,NUMEPOCH,SIZEBATCH)
close all
clc

addpath             (fullfile(matlabroot,'examples' ,'nnet'    ,'main'        )    )
addpath             (fullfile('..'      ,'..'       ,'lib'     ,'input'       ),...
                     fullfile('..'      ,'..'       ,'lib'     ,'layers'      ),...
                     fullfile('..'      ,'..'       ,'lib'     ,'models',MODEL),...
                     fullfile('..'      ,'..'       ,'lib'     ,'utilities'   )    )

PATHS              ={fullfile('..'      ,'..'       ,'data'    , DATASET      ),...
                     fullfile('..'      ,'..'       ,'networks', VER          )    }          ;
data_filename      = fullfile(PATHS{1}  ,'processed',strcat('train','.h5')    )               ;

[parameters,state] =model_parameters()                                                        ; %%% MODEL PARAMETERS

data_process(DATASET,'train',PATHS{1})                                                          %%% PRE-PROCESSING DATA 

data_store         = get_datastore_from_array( data_filename,'img',4)                         ; %%% LOAD           DATA
queue              = minibatchqueue          ( data_store                            ,...       %%% QUEUE          DATA
                                              'MiniBatchSize'  , SIZEBATCH           ,...
                                              'MiniBatchFcn'   ,@preprocessMiniBatch ,...
                                              'MiniBatchFormat', {'SSCB'}                )    ;
lineLossTrain      =training_progress()                                                       ;
iteration          =0                                                                         ;
trailingAvg        =[]                                                                        ;
trailingAvgSq      =[]                                                                        ;
start              =tic                                                                       ;
for epoch=1:NUMEPOCH                                                                            %%% TRAINING LOOP
    
    shuffle(queue)                                                                            ;
    
    while hasdata(queue)
        iteration                             =iteration + 1                                  ;
         x_train                              =next      (queue               )               ;
        [gradients ,state      ,loss]         =dlfeval   (@model_gradients,...
                                                          parameters      ,...
                                                          x_train         ,...
                                                          state               )               ;
        [parameters,trailingAvg,trailingAvgSq]=adamupdate(parameters      ,...
                                                          gradients       ,...
                                                          trailingAvg     ,...
                                                          trailingAvgSq   ,...
                                                          iteration           )               ;
        
        Duration   =duration(0,0,toc(start),'Format','hh:mm:ss')                              ; %%% MONITOR TRAINING
        addpoints(lineLossTrain,iteration,double(gather(extractdata(loss))))
        title("Epoch: " + epoch + ", Elapsed: " + string(Duration))
        drawnow
    end
    
    checkpoint(parameters,state,epoch,PATHS{2})                                                 %%% CHECKPOINT
    
    disp(['Finished',' ','Epoch',' ',num2str(epoch)])
    
end

plotter('log',{},PATHS{2},{})                                                                   %%% EXPORT LOGS

disp(' Training Finished ')

end
