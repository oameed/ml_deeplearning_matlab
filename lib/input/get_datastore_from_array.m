%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% DEEP LEARNING WITH MATLAB                           %%%
%%% INPUT FUNCTIONS                                     %%%
%%% GET DATASTORE FORM ARRAY                            %%%
%%% by: OAMEED NOAKOASTEEN                              %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function data_store=get_datastore_from_array(FILENAME,DIR,DIM)
data      =rHDF(FILENAME,DIR)                             ;
data_store=arrayDatastore(data                       ,...
                          "IterationDimension",DIM   ,...
                          "OutputType"        ,"cell",...
                          "ReadSize"          ,1         );

end
