%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% DEEP LEARNING WITH MATLAB                           %%%
%%% MODEL FUNCTIONS                                     %%%
%%% GENERATOR FUNCTION                                  %%%
%%% by: OAMEED NOAKOASTEEN                              %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [x,STATE]=generator_function(PARAMETERS,X,doTraining,STATE)

w        =PARAMETERS.generator.fc1.Weights                                               ;
b        =PARAMETERS.generator.fc1.Bias                                                  ;

x        =layer_fullyconnected(X,w,b); 

offset   =PARAMETERS.generator.batchnorm1.Offset                                         ;
scale    =PARAMETERS.generator.batchnorm1.Scale                                          ;
Mean     =STATE.generator.batchnorm1.TrainedMean                                         ;
Variance =STATE.generator.batchnorm1.TrainedVariance                                     ;
if doTraining
    [x,Mean,Variance]                         =batchnorm(x,offset,scale,Mean,Variance )  ;
    STATE.generator.batchnorm1.TrainedMean    =Mean                                      ;
    STATE.generator.batchnorm1.TrainedVariance=Variance                                  ;
else
    x                                         =batchnorm(x,offset,scale,Mean,Variance )  ;
end

x        =relu(x)                                                                        ;

sizeBATCH=size(x,2)                                                                      ;
x        =dlarray(reshape(x,[7 7 64 sizeBATCH]),'SSCB')                                  ;


w        =PARAMETERS.generator.convt1.Weights                                            ;
b        =PARAMETERS.generator.convt1.Bias                                               ;

x        =layer_convtranspose2d(x,w     ,b         ,[[0,0];[1,1]]           )            ;

offset   =PARAMETERS.generator.batchnorm2.Offset                                         ;
scale    =PARAMETERS.generator.batchnorm2.Scale                                          ;
Mean     =STATE.generator.batchnorm2.TrainedMean                                         ;
Variance =STATE.generator.batchnorm2.TrainedVariance                                     ;
if doTraining
    [x,Mean,Variance]                         =batchnorm(x,offset,scale,Mean,Variance )  ;
    STATE.generator.batchnorm2.TrainedMean    =Mean                                      ;
    STATE.generator.batchnorm2.TrainedVariance=Variance                                  ;
else
    x                                         =batchnorm(x,offset,scale,Mean,Variance )  ;
end

x        =relu(x)                                                                        ;

w        =PARAMETERS.generator.convt2.Weights                                            ;
b        =PARAMETERS.generator.convt2.Bias                                               ;

x        =layer_convtranspose2d(x,w     ,b         ,[[0,0];[1,1]]           )            ; 

offset   =PARAMETERS.generator.batchnorm3.Offset                                         ;
scale    =PARAMETERS.generator.batchnorm3.Scale                                          ;
Mean     =STATE.generator.batchnorm3.TrainedMean                                         ;
Variance =STATE.generator.batchnorm3.TrainedVariance                                     ;
if doTraining
    [x,Mean,Variance]                         =batchnorm(x,offset,scale,Mean,Variance )  ;
    STATE.generator.batchnorm3.TrainedMean    =Mean                                      ;
    STATE.generator.batchnorm3.TrainedVariance=Variance                                  ;
else
    x                                         =batchnorm(x,offset,scale,Mean,Variance )  ;
end

x        =tanh(x)                                                                        ;

end
