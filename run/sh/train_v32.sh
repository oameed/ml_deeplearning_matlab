#!/bin/bash

cd run/matlab

echo   ' MAKE DIRECTORY FOR EXPERIMENT '                'v32'
rm     -rf                                ../../networks/v32
tar    -xzf  ../../networks/v00.tar.gz -C ../../networks
mv           ../../networks/v00           ../../networks/v32

echo   ' START TRAINING FOR EXPERIMENT '                'v32'
matlab -nodisplay -nosplash -nodesktop -r "train_gan_from_tutorial('gan_from_tutorial','v32','flowers',20,128);exit;"

echo ' GENERATING PREDICTIONS '
matlab -nodisplay -nosplash -nodesktop -r "predict('gan_from_tutorial','v32','flowers');exit;"


