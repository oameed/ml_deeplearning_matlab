%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% DEEP LEARNING WITH MATLAB                           %%%
%%% LAYER FUNCTIONS                                     %%%
%%% BATCHNORM WEIGHTS                                   %%%
%%% by: OAMEED NOAKOASTEEN                              %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% OUT   : NUMBER OF OUTPUT CHANNELS  

function [offset,scale,mean,variance]=layer_batchnorm_weights(OUT)
offset  =initializeZeros([OUT 1]        );
scale   =initializeOnes ([OUT 1]        );
mean    =zeros          ( OUT,1,'single');
variance=ones           ( OUT,1,'single');
end
